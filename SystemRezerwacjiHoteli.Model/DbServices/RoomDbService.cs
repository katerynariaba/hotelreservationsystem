﻿using System.Collections.Generic;
using System.Linq;
using SystemRezerwacjiHoteli.Model.DB;
using SystemRezerwacjiHoteli.Model.Helpers;
using SystemRezerwacjiHoteli.Model.Models;

namespace SystemRezerwacjiHoteli.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę danymy o pokojach.
    /// </summary>
    public class RoomDbService
    {
        /// <summary>
        /// Zwraca listę modelu pokoju  według podanego id hotelu, typu pokoju oraz ilości osób.
        /// </summary>
        /// <param name="hotelId">Int reprezentujący id zamówienia</param>
        /// <param name="type">String reprezentujący typ pokoju</param>
        /// <param name="numberOfPeople">Int reprezentujący ilość osób</param>
        public List<RoomModel> GetAllByParametrs(int hotelId, string type, int numberOfPeople)
        {
            var rooms = new List<RoomModel>();
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Rooms.Where(r => (r.HotelId == hotelId) && (r.Type == type) && (r.NumberOfPeople == numberOfPeople)).ToList();
                rooms = result.Select(r => new RoomModel
                {
                    Id = r.Id,
                    Price = r.Price,
                    Type = r.Type,
                    Photo = new ImageConverter().FromBase64ToBitmap(r.Photo),
                    Hotel = r.Hotel.Name,
                    NumberRoom = r.NumberRoom
                }).ToList();
            }
            return rooms;
        }

        /// <summary>
        /// Zwraca id pokoju według podanego nomeru pokoju oraz id hotelu.
        /// </summary>
        /// <param name="number">Int reprezentujący nomer pokoju</param>
        /// <param name="hotelId">Int reprezentujący id hotelu</param>
        public int GetIdByParametrs(int number, int hotelId)
        {
            int id = 0;
            using (HotelDbContext context = new HotelDbContext())
            {
                id = context.Rooms.FirstOrDefault(r => (r.NumberRoom == number) && (r.HotelId == hotelId)).Id;
            }
            return id;
        }
    }
}
