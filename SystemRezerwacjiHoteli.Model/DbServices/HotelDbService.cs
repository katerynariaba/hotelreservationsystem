﻿using System.Collections.Generic;
using System.Linq;
using SystemRezerwacjiHoteli.Model.DB;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.Model.Helpers;
using SystemRezerwacjiHoteli.Model.Models;

namespace SystemRezerwacjiHoteli.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę z danymy o hotelach.
    /// </summary>
    public class HotelDbService
    {
        /// <summary>
        /// Zwraca listę wszystkich krajach zapisanych w bazie danych.
        /// </summary>
        public List<string> GetAllCountry()
        {
            var countries = new List<string>();
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Hotels.Select(r => r.Country);
                countries = result.Distinct().ToList();
                countries.Sort();
            }
            return countries;
        }

        /// <summary>
        /// Zwraca listę wszystkich miast zapisanych w bazie danych według podanego kraju.
        /// </summary>
        /// <param name="country">String reprezentujący kraj</param>
        public List<string> GetCitiesByCountry(string country)
        {
            var cities = new List<string>();
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Hotels.Where(r => r.Country == country).Select(r => r.City);
                cities = result.Distinct().ToList();
                cities.Sort();
            }
            return cities;
        }

        /// <summary>
        /// Zwraca listę wszystkich hoteli zapisanych w bazie danych według podanego miasta.
        /// </summary>
        /// <param name="city">String reprezentujący miasto</param>
        public List<HotelModel> GetAllByCity(string city)
        {
            var hotels = new List<HotelModel>();
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Hotels.Where(r => r.City == city).OrderBy(r => r.Name).ToList();
                hotels = result.Select(r => new HotelModel
                {
                    Name = r.Name,
                    Stars = r.Stars,
                    Address = r.Address,
                    Photo = new ImageConverter().FromBase64ToBitmap(r.Photo),
                }).ToList();
            }
            return hotels;
        }

        /// <summary>
        /// Zwraca hotel według podanego id pokoju.
        /// </summary>
        /// <param name="id">Int repezentujący id pokoju.</param>
        public Hotel GetClientByRoomId(int id)
        {
            Hotel hotel;
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Rooms.FirstOrDefault(r => r.Id == id).HotelId;
                hotel = context.Hotels.FirstOrDefault(r => r.Id == result);
            }
            return hotel;
        }

        /// <summary>
        /// Zwraca id hotelu według podanej nazwy.
        /// </summary>
        /// <param name="title">String reprezentujący tytuł hotelu</param>
        public int GetIdByTitle(string title)
        {
            int id = 0;
            using (HotelDbContext context = new HotelDbContext())
            {
                id = context.Hotels.FirstOrDefault(r => r.Name == title).Id;
            }
            return id;
        }
    }
}
