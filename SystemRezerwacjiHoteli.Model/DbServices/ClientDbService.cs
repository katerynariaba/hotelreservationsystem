﻿using System.Linq;
using SystemRezerwacjiHoteli.Model.DB;
using SystemRezerwacjiHoteli.Model.DomainModel;

namespace SystemRezerwacjiHoteli.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę danymy o klientach.
    /// </summary>
    public class ClientDbService
    {
        /// <summary>
        /// Dodaje nowego klienta do bazy danych.
        /// </summary>
        /// <param name="client">Klient, którego dodajemy.</param>
        public void Add(Client client)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                context.Clients.Add(client);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Sprawdza czy dany użytkownik już jest w bazie danych.
        /// </summary>
        /// <param name="client">Użytkownik, którego sprawdzamy.</param>
        /// <returns>True dane użytkownika już znajdują się w bazie danych, inaczej False.</returns>
        public bool Login(Client client)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                if (context.Clients.Any(r => (r.Email == client.Email) && (r.Password == client.Password)))
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Zwraca id użytkownika według podanego emaila.
        /// </summary>
        /// <param name="email">String reprezentujący email użytkownika</param>
        public int GetClientId(string email)
        {
            int clientId = 0;

            using (HotelDbContext context = new HotelDbContext())
            {
                clientId = context.Clients.FirstOrDefault(r => r.Email == email).Id;
            }

            return clientId;
        }

        /// <summary>
        /// Zwraca użytkownika według podanego id.
        /// </summary>
        /// <param name="id">Int repezentujący id użytkownika.</param>
        public Client GetClientById(int id)
        {
            Client client;
            using (HotelDbContext context = new HotelDbContext())
            {
                client = context.Clients.FirstOrDefault(r => r.Id == id);
            }
            return client;
        }

        /// <summary>
        /// Modyfikuję dane użytkownika.
        /// </summary>
        /// <param name="id">Int repezentujący id użytkownika.</param>
        public void Update(int clientId, Client newClient)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                Client client = context.Clients.SingleOrDefault(b => b.Id == clientId);

                client.Email = newClient.Email;
                client.FirstName = newClient.FirstName;
                client.LastName = newClient.LastName;
                client.Password = newClient.Password;
                client.PhoneNumber = newClient.PhoneNumber;

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Zwraca użytkownika według numeru telefonu.
        /// </summary>
        /// <param name="phone">String repezentujący numer telefonu użytkownika.</param>
        public Client GetClientByPhoneNumber(string phone)
        {
            Client client;
            using (HotelDbContext context = new HotelDbContext())
            {
                client = context.Clients.FirstOrDefault(r => r.PhoneNumber == phone);
            }
            return client;
        }

        /// <summary>
        /// Usuwa użytkownika z bazy danych.
        /// </summary>
        /// <param name="clent">Użytkownik, którego usuwamy.</param>
        public void DeleteUser(Client client)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                context.Clients.Attach(client);
                context.Clients.Remove(client);
                context.SaveChanges();
            }
        }
    }
}
