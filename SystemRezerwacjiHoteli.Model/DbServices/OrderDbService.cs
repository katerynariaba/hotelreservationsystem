﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemRezerwacjiHoteli.Model.DB;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.Model.Helpers;
using SystemRezerwacjiHoteli.Model.Models;

namespace SystemRezerwacjiHoteli.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę z danymy o zawowieniach.
    /// </summary>
    public class OrderDbService
    {
        /// <summary>
        /// Dodaje nowe zamówienie do bazy danych.
        /// </summary>
        /// <param name="order">Zamówienie, którego dodajemy.</param>
        public void Add(Order order)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                context.Orders.Add(order);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Zwraza id według podanych parametrów.
        /// </summary>
        /// <param name="clientId">Int reprezentujący id clienta.</param>
        /// <param name="roomId">Int reprezentujący id pokoju.</param>
        /// <param name="date">DataTime reprezentujący date zamowienia.</param>
        public int GetIdByParametrs(int clientId, DateTime date, DateTime checkInDate, DateTime endDate, int roomId)
        {
            int id = 0;
            using (HotelDbContext context = new HotelDbContext())
            {
                id = context.Orders
                    .FirstOrDefault(r => (r.ClientId == clientId) && (r.OrderDate == date) && (r.CheckInDate == checkInDate) && (r.EndDate == endDate) && (r.RoomId == roomId)).Id;
            }
            return id;
        }

        /// <summary>
        /// Zwraca listę modelu zamówienia według podanego id kienta.
        /// </summary>
        /// <param name="clientId">Int reprezentujący id klienta</param>
        public List<OrderModel> GetAllByClientId(int clientId)
        {
            var orders = new List<OrderModel>();
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Orders.Where(r => r.ClientId == clientId).ToList();
                orders = result.Select(r => new OrderModel
                {
                    Hotel = (new HotelDbService().GetClientByRoomId(r.RoomId)).Name,
                    Country = (new HotelDbService().GetClientByRoomId(r.RoomId)).Country,
                    City = (new HotelDbService().GetClientByRoomId(r.RoomId)).City,
                    Price = Convert.ToDecimal((r.EndDate - r.CheckInDate).TotalDays) * (r.Room.Price),
                    Type = r.Room.Type,
                    OderDate = r.OrderDate,
                    HotelPhoto = new ImageConverter().FromBase64ToBitmap((new HotelDbService().GetClientByRoomId(r.RoomId)).Photo)
                }).ToList();
            }
            return orders;
        }

        /// <summary>
        /// Zwraca listę modelu podtwierdzenua zamówienia według podanego id zamówienia oraz pokoju.
        /// </summary>
        /// <param name="orderId">Int reprezentujący id zamówienia</param>
        /// <param name="roomId">Int reprezentujący id pokoju</param>
        public ConfirmModel GetOrderInfo(int orderId, int roomId, int clientId)
        {
            ConfirmModel confirm;
            using (HotelDbContext context = new HotelDbContext())
            {
                var result = context.Orders.FirstOrDefault(r => r.Id == orderId);
                confirm = new ConfirmModel
                {
                    OrderId = (result.Id).ToString(),
                    OrderDate = (result.OrderDate.ToShortDateString()).ToString(),
                    LastName = (new ClientDbService().GetClientById(clientId)).LastName,
                    FirstName = (new ClientDbService().GetClientById(clientId)).FirstName,
                    ClientEmail = (new ClientDbService().GetClientById(clientId)).Email,
                    PhoneNumberClient = (new ClientDbService().GetClientById(clientId)).PhoneNumber,
                    HotelName = (new HotelDbService().GetClientByRoomId(roomId)).Name,
                    Stars = (new HotelDbService().GetClientByRoomId(roomId)).Stars,
                    PhoneNumberHotel = (new HotelDbService().GetClientByRoomId(roomId)).PhoneNumber,
                    RoomNumber = (result.Room.NumberRoom).ToString(),
                    Type = result.Room.Type,
                    NumberOfPeople = (result.Room.NumberOfPeople).ToString(),
                    CheckInDate = (result.CheckInDate.ToShortDateString()).ToString(),
                    EndDate = (result.EndDate.ToShortDateString()).ToString(),
                    Price = (Convert.ToDecimal((result.EndDate - result.CheckInDate).TotalDays) * (result.Room.Price)).ToString(),
                    Address = (new HotelDbService().GetClientByRoomId(roomId)).Address
                };
            }
            return confirm;
        }

        /// <summary>
        /// Sprawdza czy pokój jest zarezerwowany w podanych datach.
        /// </summary>
        /// <param name="from">DateTime reprezentujący datę początku</param>
        /// <param name="roomId">Int reprezentujący id pokoju</param>
        /// <param name="to">DateTime reprezentujący datę końca</param>
        /// <returns>True jeśli pokój jest zarezerwowany, inaczej False</returns>
        public bool ReservedRoom(DateTime from, DateTime to, int roomId)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                Order order = context.Orders.FirstOrDefault(r => ((r.CheckInDate >= from) && (r.EndDate <= from)) || ((r.CheckInDate >= to) && (r.EndDate <= to)) && (r.RoomId == roomId));
                if (order != null)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Zwraza zamówienie według podanych parametrów.
        /// </summary>
        /// <param name="idClient">Int reprezentujący id clienta.</param>
        /// <param name="idRoom">Int reprezentujący id pokoju.</param>
        /// <param name="OrderDate">DataTime reprezentujący date zamowienia.</param>
        public Order GetByParametrs(int idClient, int idRoom, DateTime OrderDate)
        {
            Order order = null;
            using (HotelDbContext context = new HotelDbContext())
            {
                order = context.Orders.FirstOrDefault(r => (r.ClientId == idClient)&&(r.RoomId == idRoom)&&(r.OrderDate ==OrderDate));
            }
            return order;
        }

        /// <summary>
        /// Usuwa zamówienie z bazy danych.
        /// </summary>
        /// <param name="order">zamówienie, które usuwamy.</param>
        public void DeleteOrder(Order order)
        {
            using (HotelDbContext context = new HotelDbContext())
            {
                context.Orders.Attach(order);
                context.Orders.Remove(order);
                context.SaveChanges();
            }
        }
    }
}
