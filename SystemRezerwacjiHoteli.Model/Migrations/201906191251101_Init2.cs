namespace SystemRezerwacjiHoteli.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Hotels", "Stars", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Hotels", "Stars", c => c.Int(nullable: false));
        }
    }
}
