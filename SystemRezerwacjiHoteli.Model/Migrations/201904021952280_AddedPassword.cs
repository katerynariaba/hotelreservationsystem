namespace SystemRezerwacjiHoteli.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPassword : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Password", c => c.String());
            DropColumn("dbo.Clients", "PassportNumber");
            DropColumn("dbo.Clients", "DateOfBirth");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "DateOfBirth", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clients", "PassportNumber", c => c.String());
            DropColumn("dbo.Clients", "Password");
        }
    }
}
