namespace SystemRezerwacjiHoteli.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Room_Id", "dbo.Rooms");
            DropIndex("dbo.Orders", new[] { "Room_Id" });
            RenameColumn(table: "dbo.Orders", name: "Room_Id", newName: "RoomID");
            AlterColumn("dbo.Orders", "RoomID", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "RoomID");
            AddForeignKey("dbo.Orders", "RoomID", "dbo.Rooms", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "RoomID", "dbo.Rooms");
            DropIndex("dbo.Orders", new[] { "RoomID" });
            AlterColumn("dbo.Orders", "RoomID", c => c.Int());
            RenameColumn(table: "dbo.Orders", name: "RoomID", newName: "Room_Id");
            CreateIndex("dbo.Orders", "Room_Id");
            AddForeignKey("dbo.Orders", "Room_Id", "dbo.Rooms", "Id");
        }
    }
}
