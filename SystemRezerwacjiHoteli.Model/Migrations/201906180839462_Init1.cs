namespace SystemRezerwacjiHoteli.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Rooms", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rooms", "Type", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rooms", "Type", c => c.Int(nullable: false));
            AlterColumn("dbo.Rooms", "Price", c => c.Int(nullable: false));
        }
    }
}
