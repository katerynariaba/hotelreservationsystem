namespace SystemRezerwacjiHoteli.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", new[] { "ClientID" });
            DropIndex("dbo.Orders", new[] { "RoomID" });
            CreateIndex("dbo.Orders", "ClientId");
            CreateIndex("dbo.Orders", "RoomId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Orders", new[] { "RoomId" });
            DropIndex("dbo.Orders", new[] { "ClientId" });
            CreateIndex("dbo.Orders", "RoomID");
            CreateIndex("dbo.Orders", "ClientID");
        }
    }
}
