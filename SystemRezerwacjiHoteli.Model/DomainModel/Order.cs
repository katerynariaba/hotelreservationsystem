﻿using System;

namespace SystemRezerwacjiHoteli.Model.DomainModel
{
    /// <summary>
    /// Klasa reprezentująca model zamówienia.
    /// </summary>
    public class Order
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int RoomId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Client Client { get; set; }
        public virtual Room Room { get; set; }
    }
}
