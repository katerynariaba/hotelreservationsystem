﻿using System.Collections.Generic;

namespace SystemRezerwacjiHoteli.Model.DomainModel
{
    /// <summary>
    /// Klasa reprezentująca model pokoju.
    /// </summary>
    public class Room
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public decimal Price { get; set; }
        public int NumberRoom { get; set; }
        public string Type { get; set; }
        public int NumberOfPeople { get; set; }
        public string Photo { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual Hotel Hotel { get; set; }
    }
}
