﻿using System.Collections.Generic;

namespace SystemRezerwacjiHoteli.Model.DomainModel
{
    /// <summary>
    /// Klasa reprezentująca model hotelu.
    /// </summary>
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Stars { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
    }
}
