﻿namespace SystemRezerwacjiHoteli.Model.Models
{
    /// <summary>
    /// Klasa reprezentująca model, który zawiera wszystkie potrzebne dane dla generacji pdf.
    /// </summary>
    public class ConfirmModel
    { 
        public string OrderId { get; set; }
        public string OrderDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string ClientEmail { get; set; }
        public string PhoneNumberClient { get; set; }
        public string HotelName { get; set; }
        public string Stars { get; set; }
        public string PhoneNumberHotel { get; set; }
        public string RoomNumber { get; set; }
        public string Type { get; set; }
        public string NumberOfPeople { get; set; }
        public string CheckInDate { get; set; }
        public string EndDate { get; set; }
        public string Price { get; set; }
        public string Address { get; set; }
    }
}
