﻿using System.Windows.Media.Imaging;

namespace SystemRezerwacjiHoteli.Model.Models
{
    /// <summary>
    /// Klasa reprezentująca model pokoju, który zawiera id, cenę, typ, number, zdjęcie pokoju w postaci BitmapImage oraz tytuł hotelu.
    /// </summary>
    public class RoomModel
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }
        public string Hotel { get; set; }
        public BitmapImage Photo { get; set; }
        public int NumberRoom { get; set; }
    }
}
