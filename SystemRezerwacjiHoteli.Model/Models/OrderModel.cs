﻿using System;
using System.Windows.Media.Imaging;

namespace SystemRezerwacjiHoteli.Model.Models
{
    /// <summary>
    /// Klasa reprezentująca model zamówienia, który zawiera wszystkie tytuł hotelu, kraj, miasto, cenę zamówienia, typ pokoju, zdjęcie hotelu w postaci BitmapImage oraz datę zamówienia.
    /// </summary>
    public class OrderModel
    {
        public string Hotel { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }
        public BitmapImage HotelPhoto { get; set; }
        public DateTime OderDate { get; set; }
    }
}
