﻿using System.Windows.Media.Imaging;

namespace SystemRezerwacjiHoteli.Model.Models
{
    /// <summary>
    /// Klasa reprezentująca model hotelu, który zawiera tytuł, gwiazdki, adres oraz zdjęcie w postaci BitmapImage.
    /// </summary>
    public class HotelModel
    {
        public string Name { get; set; }
        public string Stars { get; set; }
        public string Address { get; set; }
        public BitmapImage Photo { get; set; }
    }
}
