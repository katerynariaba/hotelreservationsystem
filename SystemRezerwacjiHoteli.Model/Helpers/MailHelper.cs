﻿using iTextSharp.text.pdf;
using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using SystemRezerwacjiHoteli.Model.Models;

namespace SystemRezerwacjiHoteli.Model.Helpers
{
    /// <summary>
    /// Klasa, która zawiera metodę pozwalającą wygenerować oraz wysłać na mail pdf-file.
    /// </summary>
    public class MailHelper
    {
        /// <summary>
        /// Wysyła na podany email wygenerowany pdf-file.
        /// </summary>
        /// <param name="confirm">ConfirmModel zawierający danę potrzebne dla pdf</param>
        public void SendEmail(ConfirmModel confirm)
        {
            string pdfTemplate = @"..\..\..\SystemRezerwacjiHoteli\ReservationConfirmation.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);

            #region preparepdf
            byte[] fileArray;
            using (var ms = new MemoryStream())
            {
                using (PdfStamper pdfStamper = new PdfStamper(pdfReader, ms))
                {
                    AcroFields pdfFormFields = pdfStamper.AcroFields;

                    //your booking data
                    pdfFormFields.SetField("bookingNumber", confirm.OrderId);
                    pdfFormFields.SetFieldProperty("bookingNumber", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("reservationDate", confirm.OrderDate);
                    pdfFormFields.SetFieldProperty("reservationDate", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("lastName", confirm.LastName);
                    pdfFormFields.SetFieldProperty("lastName", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("firstName", confirm.FirstName);
                    pdfFormFields.SetFieldProperty("firstName", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("emailClient", confirm.ClientEmail);
                    pdfFormFields.SetFieldProperty("clientEmail", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("telephoneClient", confirm.PhoneNumberClient);
                    pdfFormFields.SetFieldProperty("clientEmail", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    //your hotel data

                    pdfFormFields.SetField("hotelName", confirm.HotelName);
                    pdfFormFields.SetFieldProperty("hotelName", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("hotelStars", confirm.Stars);
                    pdfFormFields.SetFieldProperty("hotelStar", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("hotelAddress", confirm.Address);
                    pdfFormFields.SetFieldProperty("hotelAddress", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    if (confirm.PhoneNumberHotel != null) pdfFormFields.SetField("hotelPhone", confirm.PhoneNumberHotel);
                    else pdfFormFields.SetField("hotelPhone", "-");
                    pdfFormFields.SetFieldProperty("hotelPhone", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    //your room data
                    pdfFormFields.SetField("roomNumber", confirm.RoomNumber);
                    pdfFormFields.SetFieldProperty("roomNumber", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("roomType", confirm.Type);
                    pdfFormFields.SetFieldProperty("roomType", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("numberOfPeople", confirm.NumberOfPeople);
                    pdfFormFields.SetFieldProperty("numberOfPeople", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    //
                    pdfFormFields.SetField("checkInDate", confirm.CheckInDate);
                    pdfFormFields.SetFieldProperty("checkInDate", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("endDate", confirm.EndDate);
                    pdfFormFields.SetFieldProperty("endDate", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfFormFields.SetField("price", confirm.Price);
                    pdfFormFields.SetFieldProperty("price", "setfflags", PdfFormField.FF_READ_ONLY, null);

                    pdfStamper.FormFlattening = false;
                }

                fileArray = ms.ToArray();
            }
            #endregion

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "PDF Files|*.pdf";
            dlg.FileName = "BookingConfirmation-" + DateTime.Now.ToShortDateString() + ".pdf";
            dlg.FilterIndex = 0;

            if (dlg.ShowDialog() == true)
            {
                string newFile = dlg.FileName;

                File.WriteAllBytes(newFile, fileArray);
            }

            using (SmtpClient smtpClient = new SmtpClient("poczta.interia.pl", 587))
            {
                smtpClient.Credentials = new NetworkCredential("systemrezerwacjihoteli@interia.pl", "admin123");

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("systemrezerwacjihoteli@interia.pl", "System Rezerwacji Hoteli");
                    mail.To.Add(new MailAddress(confirm.ClientEmail));

                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;

                    mail.Subject = "Booking confirmation";
                    mail.Body =
                        $"<h2 style=\"color: #008B8B; \">Hallo,</h2> <p  style=\"font-size: 15px; \">thank you a lot for your reservation. <br><br>Hotel is booked for you. We wish you a pleasant stay!</p> <br><hr><br> <p>Date and time reservation: {DateTime.Now}</p>";
                    mail.IsBodyHtml = true;
                    var ms = new MemoryStream(fileArray);
                    mail.Attachments.Add(new Attachment(ms, "BookingConfirmation-" + DateTime.Now.ToShortDateString() + ".pdf"));
                    smtpClient.Send(mail);
                    ms.Close();
                }
            }
        }
    }
}
