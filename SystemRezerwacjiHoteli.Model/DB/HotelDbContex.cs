﻿using System.Data.Entity;
using SystemRezerwacjiHoteli.Model.DomainModel;

namespace SystemRezerwacjiHoteli.Model.DB
{
    /// <summary>
    /// Klasa, która reprezentuje DbContext.
    /// </summary>
    public class HotelDbContext : DbContext
    {
        public HotelDbContext() : base(@"Data Source=DESKTOP-5MLMB9J;Initial Catalog=HotelReservationDb;Integrated Security=True")
        {

        }

        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
