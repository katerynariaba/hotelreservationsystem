﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemRezerwacjiHoteli.Model.DbServices;

namespace SystemRezerwacjiHoteli.UnitTest
{
    [TestClass]
    public class RoomUnitTest
    {
        [TestMethod]
        public void GetAllByParametrsMethodReturnNotNull()
        {
            var service = new RoomDbService();

            var result = service.GetAllByParametrs(1, "Honeymoon", 2);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetIdByParametrsMethodReturnNotNull()
        {
            var service = new RoomDbService();

            var result = service.GetIdByParametrs(1234, 2);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetIdByParametrsMethodReturnInt()
        {
            var service = new RoomDbService();

            var result = service.GetIdByParametrs(1234, 2);

            Assert.IsInstanceOfType(result, typeof(int));
        }

        [TestMethod]
        public void GetIdByParametrsMethodTrueId()
        {
            var service = new RoomDbService();

            var result = service.GetIdByParametrs(1234, 2);

            Assert.AreEqual(1, result);
        }
    }
}
