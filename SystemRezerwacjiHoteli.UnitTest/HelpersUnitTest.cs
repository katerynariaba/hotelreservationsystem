﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemRezerwacjiHoteli.Model.Helpers;

namespace SystemRezerwacjiHoteli.UnitTest
{
    [TestClass]
    public class HelpersUnitTest
    {
        [TestMethod]
        public void EmailValidMethodReturnTrueForValidEmail()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("admin@gmail.com");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void EmailValidMethodReturnFalseForNotValidEmail()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("email");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void EmailValidMethodReturnBool()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("email");

            Assert.IsInstanceOfType(result, typeof(bool));
        }

        [TestMethod]
        public void ValueValidMethodReturnFalseFor5()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("5");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnTrueForFraction()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("7.25");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnTrueForText()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("azaza");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnBool()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("7.25");

            Assert.IsInstanceOfType(result, typeof(bool));
        }
    }
}
