﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemRezerwacjiHoteli.Model.DbServices;

namespace SystemRezerwacjiHoteli.UnitTest
{
    [TestClass]
    public class HotelUnitTest
    {
        [TestMethod]
        public void GetAllCountryMethodReturnNotNull()
        {
            var service = new HotelDbService();

            var result = service.GetAllCountry();

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetCitiesByCountryMethodReturnNotNull()
        {
            var service = new HotelDbService();

            var result = service.GetCitiesByCountry("Poland");

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetAllByCityMethodReturnNotNull()
        {
            var service = new HotelDbService();

            var result = service.GetAllByCity("New York");

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetClientByRoomIdMethodReturnNotNull()
        {
            var service = new HotelDbService();

            var result = service.GetClientByRoomId(1);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetIdByTitleMethodReturnNotNull()
        {
            var service = new HotelDbService();

            var result = service.GetIdByTitle("Plaza");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetIdByTitleMethodReturn2ForPlaza()
        {
            var service = new HotelDbService();

            var result = service.GetIdByTitle("Plaza");

            Assert.AreEqual(2, result);
        }

    }
}
