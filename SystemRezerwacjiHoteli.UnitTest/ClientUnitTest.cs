﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;

namespace SystemRezerwacjiHoteli.UnitTest
{
    [TestClass]
    public class ClientUnitTest
    {
        [TestMethod]
        public void LoginMethodReturnTrueForExistingClient()
        {
            var service = new ClientDbService();

            var client = new Client()
            {
                LastName = "Admin",
                FirstName = "Admin",
                Email = "admin@gmail.com",
                Password = "admin",
                PhoneNumber = "admin"
            };

            service.Add(client);

            var result = service.Login(client);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LoginMethodReturnFalseForNotExistingClient()
        {
            var service = new ClientDbService();

            var client = new Client()
            {
                LastName = "q",
                FirstName = "q",
                Email = "q",
                Password = "q",
                PhoneNumber = "q"
            };

            var result = service.Login(client);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AddingMethodAddClient()
        {
            var service = new ClientDbService();

            var client = new Client()
            {
                LastName = "LastName",
                FirstName = "FirstName",
                Email = "email@gmail.com",
                Password = "password",
                PhoneNumber = "Phone"
            };

            service.Add(client);

            var result = service.Login(client);

            Assert.IsTrue(result);

            service.DeleteUser(client);
        }

        [TestMethod]
        public void GetClientIdMethodReturn1ForAdmin()
        {
            var service = new ClientDbService();

            var result = service.GetClientId("admin@gmail.com");

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void GetClientIdMethodReturnInt()
        {
            var service = new ClientDbService();

            var result = service.GetClientId("admin@gmail.com");

            Assert.IsInstanceOfType(result, typeof(int));
        }

        [TestMethod]
        public void GetClientByIdMethodReturnTrueClient()
        {
            var service = new ClientDbService();

            var result = service.GetClientById(1);

            Assert.AreEqual("Admin", result.LastName);
        }

        [TestMethod]
        public void GetClientByPhoneNumberMethodReturnTrueClient()
        {
            var service = new ClientDbService();

            var result = service.GetClientByPhoneNumber("admin");

            Assert.AreEqual("Admin", result.LastName);
        }

        [TestMethod]
        public void DeleteMethodDeleteClient()
        {
            var service = new ClientDbService();

            var client = new Client()
            {
                LastName = "LastName",
                FirstName = "FirstName",
                Email = "email@gmail.com",
                Password = "password",
                PhoneNumber = "Phone"
            };

            service.Add(client);
            service.DeleteUser(client);

            var result = service.Login(client);

            Assert.IsFalse(result);
        }

    }
}
