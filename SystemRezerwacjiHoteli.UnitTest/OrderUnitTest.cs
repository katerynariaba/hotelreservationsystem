﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;

namespace SystemRezerwacjiHoteli.UnitTest
{
    [TestClass]
    public class OrderUnitTest
    {
        [TestMethod]
        public void GetByParametrsMethodReturnTrueOrder()
        {
            var service = new OrderDbService();

            var result = service.GetByParametrs(1, 1, new DateTime(2019,06,23));

            Assert.AreEqual(1, result.ClientId);
        }

        [TestMethod]
        public void GetByParametrsMethodReturnIsNotNull()
        {
            var service = new OrderDbService();

            var result = service.GetByParametrs(1, 1, new DateTime(2019, 06, 22));

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddingMethodAddOrder()
        {
            var service = new OrderDbService();

            var order = new Order()
            {
                ClientId = 1,
                RoomId = 1,
                OrderDate =DateTime.Today,
                CheckInDate = DateTime.Today,
                EndDate = DateTime.Today
            };

            service.Add(order);

            var result = service.GetByParametrs(1, 1, DateTime.Today);

            Assert.AreEqual(DateTime.Today, result.OrderDate);

            service.DeleteOrder(order);
        }

        [TestMethod]
        public void GetIdByParametrsMethodReturnTrueId()
        {
            var service = new OrderDbService();

            var result = service.GetIdByParametrs(1, new DateTime(2019, 06, 22), new DateTime(2019, 06, 22), new DateTime(2019, 06, 30), 1);

            Assert.AreEqual(21, result);
        }

        [TestMethod]
        public void GetIdByParametrsMethodReturnInt()
        {
            var service = new OrderDbService();

            var result = service.GetIdByParametrs(1, new DateTime(2019, 06, 22), new DateTime(2019, 06, 22), new DateTime(2019, 06, 30), 1);

            Assert.IsInstanceOfType(result, typeof(int));
        }

        [TestMethod]
        public void GetAllByClientIdMethodReturnNotNull()
        {
            var service = new OrderDbService();

            var result = service.GetAllByClientId(1);

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetOrderInfoMethodReturnNotNull()
        {
            var service = new OrderDbService();

            var result = service.GetOrderInfo(21, 1, 1);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReservedRoomMethodTrueForReservedRoom()
        {
            var service = new OrderDbService();

            var result = service.ReservedRoom(new DateTime(2019, 06, 25), new DateTime(2019, 06, 25), 22);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ReservedRoomMethodFalseForNotReservedRoom()
        {
            var service = new OrderDbService();

            var result = service.ReservedRoom(new DateTime(2018, 06, 22), new DateTime(2018, 06, 24), 1);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ReservedRoomMethodReturnBool()
        {
            var service = new OrderDbService();

            var result = service.ReservedRoom(new DateTime(2018, 06, 22), new DateTime(2018, 06, 24), 1);

            Assert.IsInstanceOfType(result, typeof(bool));
        }

        [TestMethod]
        public void GetByParametrsMethodReturnNotNull()
        {
            var service = new OrderDbService();

            var result = service.GetByParametrs(1, 1, DateTime.Today);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteMethodDeleteOrder()
        {
            var service = new OrderDbService();

            var order = new Order()
            {
                ClientId = 1,
                RoomId = 1,
                OrderDate = DateTime.Today,
                CheckInDate = DateTime.Today,
                EndDate = DateTime.Today
            };

            service.Add(order);
            service.DeleteOrder(order);

            var result = service.GetByParametrs(1, 1, DateTime.Today);

            Assert.IsNotNull(result);
        }


    }
}
