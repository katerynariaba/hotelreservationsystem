﻿using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.Model.Helpers;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for EditProfileWindow.xaml
    /// </summary>
    public partial class EditProfileWindow : Window
    {
        public EditProfileWindow()
        {
            InitializeComponent();

            var client = new ClientDbService().GetClientById(ClientData.Id);

            lastNameBox.Text = client.LastName;
            firstNameBox.Text = client.FirstName;
            emailBox.Text = client.Email;
            phoneBox.Text = client.PhoneNumber;
            passwordBox.Password = client.Password;
            passwordConfirmBox.Password = client.Password;

        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(emailBox.Text))
            {
                MessageBox.Show("Enter an email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailBox.Focus();
            }
            else if (!new ValidationHelper().EmailIsValid(emailBox.Text))
            {
                MessageBox.Show("Enter a valid email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailBox.Focus();
            }
            else
            {
                if (string.IsNullOrEmpty(passwordBox.Password))
                {
                    MessageBox.Show("Enter password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordBox.Focus();
                }
                else if (passwordBox.Password != passwordConfirmBox.Password)
                {
                    MessageBox.Show("Confirm password must be same as password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordConfirmBox.Focus();
                }
                else
                {
                    new ClientDbService().Update(ClientData.Id, new Client
                    {
                        LastName = lastNameBox.Text,
                        FirstName = firstNameBox.Text,
                        Email = emailBox.Text,
                        PhoneNumber = phoneBox.Text,
                        Password = passwordBox.Password
                    });

                    MessageBox.Show("You have update successfully.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
        }
    }
}
