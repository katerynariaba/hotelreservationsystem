﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.Models;
using SystemRezerwacjiHoteli.SessionData;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for HotelsWindow.xaml
    /// </summary>
    public partial class HotelsWindow : Window
    {
        public HotelsWindow()
        {
            InitializeComponent();

            List<HotelModel> hotelList = new List<HotelModel>();
            hotelList = new HotelDbService().GetAllByCity(OrderData.City);

            var source = new ObservableCollection<HotelModel>();

            foreach (var item in hotelList)
            {
                source.Add(
                new HotelModel
                {
                    Photo = item.Photo,
                    Name = item.Name,
                    Stars = item.Stars,
                    Address = item.Address
                }
                );
            }

            hotelsItemsControl.ItemsSource = source;
        }
    }
}
