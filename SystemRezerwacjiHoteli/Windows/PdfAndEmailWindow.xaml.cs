﻿using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.Helpers;
using SystemRezerwacjiHoteli.SessionData;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for PdfAndEmailWindow.xaml
    /// </summary>
    public partial class PdfAndEmailWindow : Window
    {
        public PdfAndEmailWindow()
        {
            InitializeComponent();
        }

        private void pdfButton_Click(object sender, RoutedEventArgs e)
        {
            new MailHelper().SendEmail(new OrderDbService().GetOrderInfo(OrderData.OrderId, OrderData.RoomId, ClientData.Id));
        }
    }
}
