﻿using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.Model.Helpers;

namespace SystemRezerwacjiHoteli
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void signUpButton_Click(object sender, RoutedEventArgs e)
        {
            RegistrationWindow registration = new RegistrationWindow();
            registration.Show();
            Close();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (!new ValidationHelper().EmailIsValid(emailBox.Text))
            {
                MessageBox.Show("Enter a valid email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailBox.Focus();
            }
            else
            {
                var clientExists = new ClientDbService().Login(new Client()
                {
                    Email = emailBox.Text,
                    Password = passwordBox.Password
                });

                if (clientExists)
                {
                    ClientData.Id = new ClientDbService().GetClientId(emailBox.Text);
                    LocationWindow window = new LocationWindow();
                    window.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("Sorry! Please enter existing email/password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
