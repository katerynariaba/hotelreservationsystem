﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.SessionData;
using SystemRezerwacjiHoteli.Windows;

namespace SystemRezerwacjiHoteli
{
    /// <summary>
    /// Interaction logic for LocationWindow.xaml
    /// </summary>
    public partial class LocationWindow : Window
    {
        public LocationWindow()
        {
            InitializeComponent();

            var countries = new HotelDbService().GetAllCountry();
            var source = new ObservableCollection<string>(countries);
            CoutryComboBox.ItemsSource = source;

            CityComboBox.IsEnabled = false;
        }

        private void CoutryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedCountry = this.CoutryComboBox.SelectedItem.ToString();
            var cities = new HotelDbService().GetCitiesByCountry(selectedCountry);
            var source = new ObservableCollection<string>(cities);
            CityComboBox.ItemsSource = source;
            CityComboBox.IsEnabled = true;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (CityComboBox.SelectedValue == null)
            {
                MessageBox.Show("Please select the city and country", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                OrderData.City = CityComboBox.SelectedValue.ToString();

                HotelsWindow window = new HotelsWindow();
                window.Show();
                Close();
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            EditProfileWindow window = new EditProfileWindow();
            window.Show();
        }

        private void historyButton_Click(object sender, RoutedEventArgs e)
        {
            HistoryWindow window = new HistoryWindow();
            window.Show();
        }
    }
}
