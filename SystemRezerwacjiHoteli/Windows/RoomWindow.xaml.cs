﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SystemRezerwacjiHoteli.Model.Helpers;
using SystemRezerwacjiHoteli.SessionData;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow : Window
    {
        public RoomWindow()
        {
            InitializeComponent();

            List<string> roomsType = new List<string>() {
                "Apartment", "Balcony Room", "Business Room", "Connected Room", "Corner", "Duplex", "Family Room", "Honeymoon", "President Room", "Studio", "Suite",};

            typeComboBox.ItemsSource = roomsType;
            typeComboBox.SelectedIndex = 0;

            toDate.IsEnabled = false;
            toDate.SelectedDate = fromDate.SelectedDate;

            fromDate.SelectedDate = DateTime.Today;
            toDate.SelectedDate = DateTime.Today;

        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBox.Text))
            {
                MessageBox.Show("Enter number of people.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                textBox.Focus();
            }
            else if (new ValidationHelper().ValueIsValid(textBox.Text))
            {
                MessageBox.Show("Enter a valid number.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                textBox.Focus();
            }
            else
            {
                OrderData.NumberOfPeople = Convert.ToInt16(textBox.Text);
                OrderData.RoomType = typeComboBox.SelectedValue.ToString();

                OrderData.CheckInDate = fromDate.SelectedDate.Value;
                OrderData.EndDate = toDate.SelectedDate.Value;

                ReservationWindow window = new ReservationWindow();
                window.Show();
            }
        }

        private void oneDayRadioButton_Click(object sender, RoutedEventArgs e)
        {
            toDate.IsEnabled = false;
            toDate.SelectedDate = fromDate.SelectedDate;
        }

        private void severalDaysRadioButton_Click(object sender, RoutedEventArgs e)
        {
            toDate.IsEnabled = true;
        }

        private void fromDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            toDate.SelectedDate = fromDate.SelectedDate;
        }
    }
}
