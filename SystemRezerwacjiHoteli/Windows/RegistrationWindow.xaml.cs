﻿using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.Model.Helpers;


namespace SystemRezerwacjiHoteli
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void logInButton_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow login = new LoginWindow();
            login.Show();
            Close();
        }

        private void singupButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(emailRegistrBox.Text))
            {
                MessageBox.Show("Enter an email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailRegistrBox.Focus();
            }
            else if (!new ValidationHelper().EmailIsValid(emailRegistrBox.Text))
            {
                MessageBox.Show("Enter a valid email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailRegistrBox.Focus();
            }
            else
            {
                if (string.IsNullOrEmpty(passwordRegistrBox.Password))
                {
                    MessageBox.Show("Enter password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordRegistrBox.Focus();
                }
                else if (passwordRegistrBox.Password != passwordConfirmBox.Password)
                {
                    MessageBox.Show("Confirm password must be same as password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordConfirmBox.Focus();
                }
                else
                {
                    new ClientDbService().Add(new Client()
                    {
                        LastName = lastNameBox.Text,
                        FirstName = firstNameBox.Text,
                        Email = emailRegistrBox.Text,
                        PhoneNumber = phoneBox.Text,
                        Password = passwordRegistrBox.Password
                    });

                    MessageBox.Show("You have Registered successfully.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    LoginWindow login = new LoginWindow();
                    login.Show();
                    Close();
                }
            }
        }
    }
}

