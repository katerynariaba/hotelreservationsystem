﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.Models;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for HistoryWindow.xaml
    /// </summary>
    public partial class HistoryWindow : Window
    {
        public HistoryWindow()
        {
            InitializeComponent();

            List<OrderModel> roomList = new List<OrderModel>();
            roomList = new OrderDbService().GetAllByClientId(ClientData.Id);

            var source = new ObservableCollection<OrderModel>();

            foreach (var item in roomList)
            {
                source.Add(
                new OrderModel
                {
                    Hotel = item.Hotel,
                    HotelPhoto = item.HotelPhoto,
                    Type = item.Type,
                    Price = item.Price,
                    Country = item.Country,
                    City = item.City,
                    OderDate = item.OderDate
                }
                );
            }

            ordersItemsControl.ItemsSource = source;
        }
    }
}
