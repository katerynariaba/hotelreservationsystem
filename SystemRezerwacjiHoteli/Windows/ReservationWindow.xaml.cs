﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.Models;
using SystemRezerwacjiHoteli.SessionData;

namespace SystemRezerwacjiHoteli.Windows
{
    /// <summary>
    /// Interaction logic for ReservationWindow.xaml
    /// </summary>
    public partial class ReservationWindow : Window
    {
        public ReservationWindow()
        {
            InitializeComponent();     
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<RoomModel> roomList = new List<RoomModel>();
            roomList = new RoomDbService().GetAllByParametrs(OrderData.HotelId, OrderData.RoomType, OrderData.NumberOfPeople);

            var source = new ObservableCollection<RoomModel>();

            foreach (var item in roomList)
            {
                if (!new OrderDbService().ReservedRoom(OrderData.CheckInDate, OrderData.EndDate, item.Id))
                {
                    source.Add(
                    new RoomModel
                    {
                        Photo = item.Photo,
                        Type = item.Type,
                        Price = item.Price,
                        Hotel = item.Hotel,
                        Id = item.Id
                    }
                    );
                }
            }

            if (source.Count() == 0)
            {
                MessageBox.Show("All rooms with selected parameters are not available or reserved for this period of time. Please choose another room.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                Close();
            }
            else
            {
                roomsItemsControl.ItemsSource = source;
            }
        }
    }
}
