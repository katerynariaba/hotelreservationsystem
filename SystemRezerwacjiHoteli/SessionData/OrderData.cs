﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemRezerwacjiHoteli.SessionData
{
   public static class OrderData
        {
            private static string city;
            private static int hotelId;
            private static int numberOfPeople;
            private static string roomType;
            private static DateTime checkInDate;
            private static DateTime endDate;
            private static int rommId;
            private static int orderId;


        public static string City
            {
                get { return city; }
                set
                {
                    city = value;
                }
            }

            public static int HotelId
        {
                get { return hotelId; }
                set
                {
                hotelId = value;
                }
            }

        public static string RoomType
        {
            get { return roomType; }
            set
            {
                roomType = value;
            }
        }

        public static int NumberOfPeople
        {
            get { return numberOfPeople; }
            set
            {
                numberOfPeople = value;
            }
        }

        public static DateTime CheckInDate
        {
            get { return checkInDate; }
            set
            {
                checkInDate = value;
            }
        }

        public static DateTime EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
            }
        }

        public static int RoomId
        {
            get { return rommId; }
            set
            {
                rommId = value;
            }
        }

        public static int OrderId
        {
            get { return orderId; }
            set
            {
                orderId = value;
            }
        }
    }
    
}
