﻿using System;

namespace SystemRezerwacjiHoteli
{
    public static class ClientData
    {
        private static int id;
        private static bool idWasSet;

        public static int Id
        {
            get { return id; }
            set
            {
                if (!idWasSet)
                {
                    id = value;
                    idWasSet = true;
                }
                else
                {
                    throw new InvalidOperationException("Client id can be assigned only once");
                }
            }
        }
    }
}
