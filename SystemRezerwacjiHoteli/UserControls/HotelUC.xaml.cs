﻿using System.Windows;
using System.Windows.Controls;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.SessionData;
using SystemRezerwacjiHoteli.Windows;

namespace SystemRezerwacjiHoteli.UserControls
{
    /// <summary>
    /// Interaction logic for HotelUC.xaml
    /// </summary>
    public partial class HotelUC : UserControl
    {
        public HotelUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OrderData.HotelId = new HotelDbService().GetIdByTitle(hotelName.Content.ToString());

            RoomWindow window = new RoomWindow();
            window.Show();
        }
    }
}
