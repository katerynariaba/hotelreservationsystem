﻿using System;
using System.Windows;
using System.Windows.Controls;
using SystemRezerwacjiHoteli.Model.DbServices;
using SystemRezerwacjiHoteli.Model.DomainModel;
using SystemRezerwacjiHoteli.SessionData;
using SystemRezerwacjiHoteli.Windows;

namespace SystemRezerwacjiHoteli.UserControls
{
    /// <summary>
    /// Interaction logic for RoomUC.xaml
    /// </summary>
    public partial class RoomUC : UserControl
    {
        public RoomUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new OrderDbService().Add(new Order()
            {
                ClientId = ClientData.Id,
                OrderDate = DateTime.Today,
                CheckInDate = OrderData.CheckInDate,
                EndDate = OrderData.EndDate,
                RoomId = Convert.ToInt16(idRoom.Content)
            });

            OrderData.RoomId = Convert.ToInt16(idRoom.Content);
            OrderData.OrderId = new OrderDbService().GetIdByParametrs(ClientData.Id, DateTime.Today, OrderData.CheckInDate, OrderData.EndDate, OrderData.RoomId);

            PdfAndEmailWindow window = new PdfAndEmailWindow();
            window.Show();
        }
    }
}
